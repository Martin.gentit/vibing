using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class SetupCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<CinemachineVirtualCamera>().m_Follow = GameObject.Find("Car").transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
