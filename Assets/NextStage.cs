using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class NextStage : MonoBehaviour
{
    public string currentScene;
    public string onTouchLoadScene;
    private GameObject car;
    private BoxCollider2D _carCollider;
    private BoxCollider2D _carRlTire;
    private BoxCollider2D _carRrTire;
    private BoxCollider2D _spot;
    
    // Start is called before the first frame update
    void Start()
    {
        car = GameObject.Find("Car");
        _carCollider = car.GetComponent<BoxCollider2D>();
        _carRlTire = GameObject.Find("RL_tire").GetComponent<BoxCollider2D>();
        _carRrTire = GameObject.Find("RR_tire").GetComponent<BoxCollider2D>();
        _spot = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_spot.IsTouching(_carCollider) && _spot.IsTouching(_carRlTire) && _spot.IsTouching(_carRrTire))
        {
            SceneManager.UnloadSceneAsync(currentScene);
            SceneManager.LoadScene(onTouchLoadScene, LoadSceneMode.Additive);
            TrailRenderer leftTrail = _carRlTire.GetComponent<TrailRenderer>();
            TrailRenderer rightTrail = _carRrTire.GetComponent<TrailRenderer>();
            leftTrail.emitting = false;
            leftTrail.Clear();
            rightTrail.emitting = false;
            rightTrail.Clear();
            car.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            car.transform.rotation = Quaternion.identity;
            car.transform.position = new Vector3(0,0,0);
        }
    }
}
