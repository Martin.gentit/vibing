using System;
using TMPro;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Rigidbody2D _rb;
    public float slipPower = 0.97f;
    public float engineBrakingFactor = 5f;
    public float brakingPower = 10f;
    public float slowTurnLimit = 8f;
    public float accelerationFactor = 2f;
    public float turnAngle = 3f;
    public TMP_Text gearing;
    public TMP_Text speedometer;
    public TrailRenderer RR_tire;
    public TrailRenderer RL_tire;
    private int currentGear;
    private int maxGear = 5;
    private int speedCap;
    private float currentSpeed;

    // Start is called before the first frame update

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        RL_tire.emitting = RR_tire.emitting = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && currentGear < maxGear)
        {
            currentGear += 1;
            if (currentGear == 0)
                speedCap = 0;
            else
                speedCap += 30;
            gearing.SetText(currentGear.ToString());
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && currentGear > -1)
        {
            currentGear -= 1;
            if (currentGear == 0)
                speedCap = 0;
            else
                speedCap -= 30;
            gearing.SetText(currentGear.ToString());
        }
        
        if (Input.GetKey(KeyCode.Z))
        {
            currentSpeed = Vector2.Dot(transform.up, _rb.velocity);
            if (currentGear > 0 && currentSpeed < speedCap)
                _rb.AddForce(transform.up * accelerationFactor);
            else if (currentGear == -1 && (currentSpeed > -10))
                _rb.AddForce(-transform.up * accelerationFactor / 2);
        }
        else if (_rb.velocity.magnitude > 0)
        {
            _rb.AddForce(-_rb.velocity.normalized * (engineBrakingFactor * (currentGear / maxGear))); // FIXME engine braking
        }
        
        if (Input.GetKey(KeyCode.S) && _rb.velocity.magnitude > 0)
        {
            _rb.AddForce(_rb.velocity.normalized * -brakingPower);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            Drift();
            Turn(false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Drift();
            Turn(true);
        }
        
        speedometer.SetText($"{(int)Vector2.Dot(transform.up, _rb.velocity)}");
    }

    private void Turn(bool turnRight)
    {
        float minTurnFactor = _rb.velocity.magnitude / slowTurnLimit;
        minTurnFactor = Mathf.Clamp01(minTurnFactor);

        if (turnRight)
        {
            _rb.MoveRotation(transform.rotation.eulerAngles.z + (-turnAngle * minTurnFactor));
        }
        else
        {
            _rb.MoveRotation(transform.rotation.eulerAngles.z + (turnAngle * minTurnFactor));
        }

    }

    private void Drift()
    {
        Vector3 forwardSpeed = transform.up * Vector2.Dot(_rb.velocity, transform.up);
        Vector3 sidewaysSpeed = transform.right * Vector2.Dot(_rb.velocity, transform.right);
        _rb.velocity = forwardSpeed + sidewaysSpeed * slipPower;
        if (Mathf.Abs(Vector2.Dot(transform.right, _rb.velocity)) > 4f)
        {
            RL_tire.emitting = RR_tire.emitting = true;
        }
        else
        {
            RL_tire.emitting = RR_tire.emitting = false;
        }
    }
}
