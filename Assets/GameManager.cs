using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public BoxCollider2D car;
    public BoxCollider2D nextStagePark;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start()
    {
        SceneManager.LoadScene("ParkingLot", LoadSceneMode.Additive);
    }

    // Update is called once per frame
    void Update()
    {
        if (car.IsTouching(nextStagePark))
        {
            SceneManager.LoadScene("Stage_one", LoadSceneMode.Additive);
        }
    }
}
