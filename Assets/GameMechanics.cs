
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMechanics : MonoBehaviour
{
    public Slider angleGauge;
    public float slipAngleBoost = 0.1f;
    public float overdriftMalus = 0.1f;
    public float minSlipAngle = 0f;
    public float maxSlipAngle = 6f;
    public float overdriftAngle = 35f;
    private float _slipAngle;
    private bool _isInSlipAngle;
    private bool _isInOverdrift;
    private Movement _movement;
    private Rigidbody2D _rb;
    
    void Awake()
    {
        DontDestroyOnLoad(GameObject.Find("Car"));
        _movement = GetComponent<Movement>();
        _rb = GetComponent<Rigidbody2D>();
        SceneManager.LoadScene("Scenes/ParkingLot", LoadSceneMode.Additive);
    }

    // Update is called once per frame
    void Update()
    {
        _slipAngle = Vector2.Angle(_rb.velocity, transform.up);
        angleGauge.value = _slipAngle;
        if (!_isInSlipAngle && _slipAngle > minSlipAngle && _slipAngle < maxSlipAngle)
        {
            _movement.accelerationFactor += slipAngleBoost;
            _isInSlipAngle = true;

        }
        else if (_isInSlipAngle && (_slipAngle < minSlipAngle || _slipAngle > maxSlipAngle))
        {
            _movement.accelerationFactor -= slipAngleBoost;
            _isInSlipAngle = false;
        }
        
        if (!_isInOverdrift && _slipAngle > overdriftAngle)
        {
            _movement.accelerationFactor -= overdriftMalus;
            _isInOverdrift = true;

        }
        else if (_isInOverdrift && (_slipAngle < overdriftAngle))
        {
            _movement.accelerationFactor += overdriftMalus;
            _isInOverdrift = false;
        }
    }
}
